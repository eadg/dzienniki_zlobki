#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      wzzjaro
#
# Created:     19-07-2012
# Copyright:   (c) wzzjaro 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import shutil
import os

year="2014-2015"


def cp_2gr():
    for i in [9]:
        print("zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')
        shutil.copy("c:\\Dziennik_Elektroniczny"+"\\zl_2g_zbiorczy_2014-15.ods", "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')

def cp_3gr():
    for i in [2,3,4,5,6,7,11,12,13]:
        print("zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')
        shutil.copy("c:\\Dziennik_Elektroniczny"+"\\zl_3g_zbiorczy_2014-15.ods", "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')

def cp_4gr():
    for i in [1,8,10]:
        print("zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')
        shutil.copy("c:\\Dziennik_Elektroniczny"+"\\zl_4g_zbiorczy_2014-15.ods", "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')

def cp_7gr():
     for i in range(14, 15):
         print("zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')
         shutil.copy("c:\\Dziennik_Elektroniczny"+"\\zl_7g_zbiorczy_2014-15.ods", "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')

def cp_8gr():
    for i in range(15, 16):
        print("zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')
        shutil.copy("c:\\Dziennik_Elektroniczny"+"\\zl_8g_zbiorczy_2014-15.ods", "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_zbiorczy"+"_"+year+'.ods')


def main():
    cp_2gr()
    cp_3gr()
    cp_4gr()
    cp_7gr()
    cp_8gr()


if __name__ == '__main__':
    main()
