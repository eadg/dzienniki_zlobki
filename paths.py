from platform import system
from os.path import isfile
import glob


class checkos(object):

    def __init__(self):
        self.nameos = system()

    def getpath(self):
        if self.nameos == 'Linux':
            return 'upload/'
        else:
            return r'i:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\wzz\\bip\\2014\\__aaa__python_aaa__\\'


class duplicate(object):

    def __init__(self, name):
        self.fname = name

    def isfile(self):
        if isfile(self.fname) is True:
            print("file exists")
            return True
        else:
            return False

    def getdupname(self, inputname):
        path = checkos().getpath()
        fname = str(inputname).split('.')[0]
        listf = glob.glob(path+fname+'*')
        return listf
