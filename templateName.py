templatyPDF = ("Ogloszenie", "SIWZ", "Zmodyfikowany_SIWZ", "Wybor", "Odpowiedzi_na_pytania",
               "Ogloszenie_o_zmianie_ogloszenia", "Formularz_asortymentowo-cenowy_zalacznik_nr_2_do_SIWZ",
               "Pytania_i_odpowiedzi", "STWiOR", "Zmiana_tresci_SIWZ", "Uniewaznienie", "Wybor_i_uniewaznienie",
               "Zalacznik_nr_8_do_SIWZ_orientacyjny_zakres_opracowania", "Informacja_o_poprawie_oczywistej_omylki_pisarskiej", "Wybor_i_Uniewaznienie",
               "Ogloszenie_o_udzieleniu_zamowienia", "Wybor_dla_zadania_nr_", "Ponowny_wybor_dla_zadania_nr_", "Zalacznik_nr_XY_do_STWiOU",
               "Sprostowanie_wyboru_dla_zadania_nr_", "Wyjasnienie_tresci_SIWZ",)
templatyDOC =  ("SIWZ_wersja_edytowalna", "STWiOR_wersja_edytowalna",)
templatyXLS =  ("Formularz_asortymentowo-cenowy_zalacznik_nr_2_do_SIWZ-wersja_edytowalna", "Zmodyfikowany_formularz_asortymentowo-cenowy_zalacznik_nr_2_do_SIWZ-wersja_edytowalna",)

templatyDIC = dict(pdf=templatyPDF, doc=templatyDOC, docx=templatyDOC, xls=templatyXLS, xlsx=templatyXLS)
