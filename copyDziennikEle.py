#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      wzzjaro
#
# Created:     19-07-2012
# Copyright:   (c) wzzjaro 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import shutil
import os

year="2015"
file="dziennik_2015_7.ods"

def cp_2gr():
    for i in [9]:
        for j in range(1, 3):
            print("zl_"+str(i)+"_gr_"+str(j)+'.ods')
            try:
                os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+'.ods')
            except:
                pass
            shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+"_"+year+'.ods')

def cp_3gr():
    for i in [2,3,4,5,6,7,11,12,13]:
        for j in range(1, 4):
            print("zl_"+str(i)+"_gr_"+str(j)+'.ods')
            try:
                os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+'.ods')
            except:
                pass
            shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+"_"+year+'.ods')

def cp_4gr():
    for i in [1,8,10]:
        for j in range(1, 5):
            print("zl_"+str(i)+"_gr_"+str(j)+'.ods')
            try:
                os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+'.ods')
            except:
                pass
            shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+"_"+year+'.ods')

def cp_7gr():
    ab="AB"
    abc="ABC"
    for i in [14]:
        for j in range(1, 3):
            for k in ab:
                print("zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                try:
                    os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                except:
                    pass
                shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+"_"+year+'.ods')

    for i in [14]:
        for j in range(3, 4):
            for k in abc:
                print("zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                try:
                    os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                except:
                    pass
                shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+"_"+year+'.ods')

def cp_8gr():
    ab="AB"
    abcd="ABCD"
    for i in [15]:
        for j in range(1, 3):
            for k in ab:
                print("zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                try:
                    os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                except:
                    pass
                shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+"_"+year+'.ods')

    for i in [15]:
        for j in range(3, 4):
            for k in abcd:
                print("zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                try:
                    os.remove("c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+'.ods')
                except:
                    pass
                shutil.copy("I:\\backup\\mojeNaDomku\\Jakub_Rokicki\\privat\\dziennikEle\\robol_2015_2016\\"+file, "c:\\Dziennik_Elektroniczny"+"\\zl_"+str(i)+"_gr_"+str(j)+k+"_"+year+'.ods')


def main():
    cp_2gr()
    cp_3gr()
    cp_4gr()
    cp_7gr()
    cp_8gr()


if __name__ == '__main__':
    main()
