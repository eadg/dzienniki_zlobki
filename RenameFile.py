import os
import sys
import re
from shutil import copy
from templateName import templatyDIC
import paths


def foo():
    pass


def listFile(path):
    lista = os.listdir(path)
    return lista


def filtr(lis=[]):
    paternc = re.compile(r"\.(xlsx|xls|docx|doc|pdf)", re.IGNORECASE)
    lisek = []
    for el in lis:
        m = paternc.search(el)
        if m:
            lisek.append(el)
    return(lisek)


def nameExt(file=""):
    paternc = re.compile(r"(.*)\.(.{3,4}$)", re.IGNORECASE)
    m = paternc.match(file)
    return(m.group(2))


def renameMe(filet="", numberAuction=0, numberAnswer=0, ext=""):
    filenew = "zp" + str(numberAuction) + "-2015_" + templatyDIC[
        ext][int(numberAnswer)] + "." + ext
    #Uversion:
    path = paths.checkos().getpath()
    # duplikaty:
    duplic = paths.duplicate(path + filenew)
    if duplic.isfile():
        pass
    else:
        print("file rename")
        copy(filet, path + filenew)
        os.remove(filet)


def main():
    try:
        path = sys.argv[1]
    except IndexError:
        path = '.'

    listFiles = filtr(listFile(path))
    answerDIC = templatyDIC

    numberAuction = input('podaj nr przetargu--> ')

    if len(listFiles) > 0:
        for filet in listFiles:
            print(filet)
            print("-"*40)
            ext = nameExt(filet)
            for stringAnswer in answerDIC[ext]:
                print(answerDIC[ext].index(stringAnswer), stringAnswer)
            inputto = input('--> ')
            if 'q' == inputto:
                break
            try:
                numberAnswer = int(inputto)
            except ValueError:
                if inputto == 'a':
                    print("aaaa")
                    continue
                else:
                    continue
            print("*"*40)
            renameMe(filet, numberAuction, numberAnswer,  ext)
    else:
        print("No files to rename! Please give me some bits :)")

if __name__ == '__main__':
    main()
