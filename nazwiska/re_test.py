#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      wzzjaro
#
# Created:     04-07-2012
# Copyright:   (c) wzzjaro 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import re

def main():
    patern = re.compile('([a-zA-Z0-9-]+)([ ]+)([a-zA-Z0-9-]+)')
    line="Ala     Ma-Kota                       ="
    m=patern.match(line)
    print(m.group(3), m.group(1))

if __name__ == '__main__':
    main()
