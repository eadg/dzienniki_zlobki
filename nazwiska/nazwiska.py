#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      wzzjaro
#
# Created:     04-07-2012
# Copyright:   (c) wzzjaro 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import re

def main():
    patern = re.compile('([\w\-]+)\W+([\w\-]+)')
    f=open("naz.txt")
    for line in f:
        m=patern.match(line)
        if m:
            print(m.group(2), m.group(1))
        else:
            pass

if __name__ == '__main__':
    main()
